"""
Please enter: python3 predict_bigimg.py img
"""
import argparse
import os
import sys
from pathlib import Path
import cv2
import tensorflow as tf
import numpy as np
from binary_train import CNNModel

stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')
from keras import callbacks, utils, layers, models
from model import ResNet50, ResNet50MGPU
from keras.utils import Sequence
sys.stderr = stderr


def parse_args(args):
    """ Add argument for selecting Data files """
    parser = argparse.ArgumentParser(description="Predictor for Leaf deseases")
    parser.add_argument('img_path', help="Path to image")
    return parser.parse_args(args)


def read_prepare_img(img_path):
    """
    Input: img_path (string)
    Output: img (BGR with subtract mean from ImageNet)
    """
    img = cv2.imread(img_path)
    img = cv2.resize(img, None, fx=0.5, fy=0.5)
    mean = np.array([103.939, 116.779, 112.68])  # ImageNet means
    img = img[..., :] - mean[:]
    return img


def generate_windows(img, step_size=32, window_size=224):
    """
    Input: img (BGR), step_size (int), window_size (int)
    Output: windows (array of BGR), x_y_s array of x, y
    """
    height, length, _ = img.shape
    windows = []
    x_y_s = []
    # (_y, _x) is upper left corner of box.
    # Image last pixel is (height -1, length -1)
    for _y in range(0, height - window_size, step_size):
        for _x in range(0, length - window_size, step_size):
            window = img[_y:_y + window_size, _x:_x + window_size]
            windows.append(window)
            x_y_s.append((_x, _y))
    return np.array(windows), np.array(x_y_s)


class Detector(object):
    """ 2 steps detection for leaf """

    def __init__(self):
        self.model = None

    def create_model(self, weights_path, numb_class=2):
        """ create model either for leaf/non leaf or 7 classification """
        base_model = ResNet50(
            include_top=False,
            weights='imagenet',
            pooling='avg',
            input_shape=(224, 224, 3))
        x = base_model.output
        x = layers.Dense(1024, activation='relu')(x)
        predictions = layers.Dense(numb_class, activation='softmax')(x)
        my_model = models.Model(inputs=base_model.input, outputs=predictions)
        for layer in my_model.layers:
            layer.trainable = True
        # callback to save best weights
        my_model.compile(
            optimizer='adam',
            loss='categorical_crossentropy',
            metrics=['accuracy'])
        self.model = my_model
        if Path(weights_path).is_file():
            print("load existing weights")
            self.model.load_weights(weights_path)

    def predict_single_img(self, img):
        """ predict leaves on single img """
        windows, x_y_s = generate_windows(img, step_size=16)
        binary_prediction = self.model.predict(
            windows, batch_size=128, verbose=1)
        x2_y2 = x_y_s + 224  # sorry, fixed size window
        new_boxes = np.concatenate((x_y_s, x2_y2, binary_prediction), axis=1)
        return new_boxes


def main(args=None):
    """ Main program """
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # Dynamic memory allocation
    sess = tf.Session(config=config)

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    img_path = args.img_path
    my_detector = Detector()
    my_detector.create_model('./7_plus_bg_best_weights.hdf5', numb_class=8)
    img = read_prepare_img(img_path)
    result = my_detector.predict_single_img(img)
    np.save('result.npy', result)


if __name__ == '__main__':
    sys.exit(main())
