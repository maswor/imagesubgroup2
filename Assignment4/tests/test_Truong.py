from pathlib import Path
import pytest
from ..train import generate_xy_list
from ..sequence import DataSequence


@pytest.fixture
def setup():
    path = Path('/media/maswor/YOLO/ME592X/Leaf_Images/')
    x_list, y_list = generate_xy_list(path)
    return x_list, y_list


class TestClass(object):

    def test_DataSequence(self):
        x_list, y_list = setup()
        my_seq = DataSequence(x_list, y_list, batch_size=40)
        my_list = my_seq[300]
        assert len(my_list[1]) == 40
