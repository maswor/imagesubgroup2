"""
Please enter: python3 draw_bigimg.py img
"""
import sys
import argparse
import numpy as np
import cv2


def non_max_suppression_fast(boxes, overlap_thresh=0.8, max_boxes=300):
    """
    Input: boxes[x1, y1, x2, y2, prob]
    Output: choosen boxes
    """
    boxes = np.array(boxes)
    np.testing.assert_array_less(boxes[:, 0], boxes[:, 2])
    np.testing.assert_array_less(boxes[:, 1], boxes[:, 3])

    if boxes.dtype.kind == 'i':
        boxes = boxes.astype('float')

    pick = []
    area = (boxes[:, 2] - boxes[:, 0]) * (boxes[:, 3] - boxes[:, 1])
    indexes = np.argsort(boxes[:, -1])
    while indexes.any():
        last = len(indexes) - 1
        idx = indexes[last]
        pick.append(idx)
        xx1_int = np.maximum(boxes[idx, 0], boxes[indexes[:last], 0])
        yy1_int = np.maximum(boxes[idx, 1], boxes[indexes[:last], 1])
        xx2_int = np.minimum(boxes[idx, 2], boxes[indexes[:last], 2])
        yy2_int = np.minimum(boxes[idx, 3], boxes[indexes[:last], 3])

        ww_int = np.maximum(0, xx2_int - xx1_int)
        hh_int = np.maximum(0, yy2_int - yy1_int)

        area_int = ww_int * hh_int
        area_union = area[idx] + area[indexes[:last]] - area_int
        overlap = area_int / (area_union + 1e-6)  # overlab ratio
        to_be_removed = np.where(
            overlap > overlap_thresh)[0]  # result is idx of "indexes"
        to_be_removed = np.concatenate(([last], to_be_removed))
        indexes = np.delete(indexes, to_be_removed)
        if len(pick) >= max_boxes:
            break
    # return boxes that were picked
    boxes = boxes[pick].astype("int")
    return boxes


def find_boxes_for_class(result_array, clf=0, thresh=0.9):
    """
    Input: clf, classification number (from 0 t0 7)
           thresh: confident (0 -> 1)
    Output: boxes that satisfy requirement
    """

    boxes_idx = np.where(result_array[:, 4 + clf] > thresh)[0]
    realbox = result_array[boxes_idx]
    box_for_nms = np.concatenate(
        (realbox[:, :4], np.expand_dims(realbox[:, 4 + clf], axis=1)), axis=1)

    return box_for_nms


def draw_boxes_in_img(img, boxes, overlap_thresh=0.3):
    """
    Input: overlap_thresh: threshold for overlaped between boxes
           boxes: (x1, y1, x2, y2, prob)
    """
    boxes_to_drawn = non_max_suppression_fast(
        boxes, overlap_thresh=overlap_thresh)
    for box in boxes_to_drawn:
        cv2.rectangle(img, (box[0], box[1]), (box[2], box[3]), (0, 0, 255), 3)

    return img


def parse_args(args):
    """ Add argument for selecting Data files """
    parser = argparse.ArgumentParser(description="Predictor for Leaf deseases")
    parser.add_argument('img_path', help="Path to image")
    parser.add_argument(
        'result_path', help="Path to result file from predict_bigimg")
    parser.add_argument(
        'classification',
        type=int,
        help="leaf classification to find (start from 0)")
    parser.add_argument(
        'output_name', default='output.jpg', help="Path to save output")
    return parser.parse_args(args)


def main(args=None):
    """ Main program """
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    img_path = args.img_path
    result_path = args.result_path
    output_name = args.output_name
    clf = args.classification

    img = cv2.imread(img_path)
    img = cv2.resize(img, None, fx=0.5, fy=0.5)
    result = np.load(result_path)
    boxes = find_boxes_for_class(result, clf=clf, thresh=0.9)
    img = draw_boxes_in_img(img, boxes, overlap_thresh=0.3)
    cv2.imwrite(output_name, img)


if __name__ == '__main__':
    sys.exit(main())
