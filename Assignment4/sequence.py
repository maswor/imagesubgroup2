"""
Sequence Data Container instead of ImageGenerator
Safer for Multiprocesses
"""
import random
import multiprocessing as mp
import itertools

import sys
import os
stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')

from keras.utils import Sequence
from keras.preprocessing import image

sys.stderr = stderr

import cv2
import numpy as np


def read_prepare_img(img_path, size=224):
    """
    function to prepare image for pre-train model
    Note: pretrain from Keras came fram Caffee with BGR input
    """
    img = cv2.imread(img_path)
    old_size = img.shape[:2]
    ratio = size / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])
    img = cv2.resize(
        img, dsize=(new_size[1], new_size[0]), interpolation=cv2.INTER_AREA)
    pad_vert_0 = (size - new_size[0]) // 2
    pad_vert_1 = (size - new_size[0]) - pad_vert_0
    pad_hori_0 = (size - new_size[1]) // 2
    pad_hori_1 = (size - new_size[1]) - pad_hori_0
    img = cv2.copyMakeBorder(img, pad_vert_0, pad_vert_1, pad_hori_0,
                             pad_hori_1, cv2.BORDER_REFLECT)
    mean = np.array([103.939, 116.779, 112.68])  # ImageNet means
    img = img[..., :] - mean[:]
    return img


def random_transform(img,
                     rotation_range=90.,
                     shear_range=20.,
                     width_shift_range=0.2,
                     height_shift_range=0.2,
                     zoom_range=[0.8, 1.2],
                     channel_shift_range=0.1):
    """
    emulating std::function in c++
    a single function which dispatch to diffirent functions
    """

    def rotate(img):
        """ rotate img randomly in range of [rotation_range] """
        return image.random_rotation(
            img,
            rotation_range,
            row_axis=0,
            col_axis=1,
            channel_axis=2,
            fill_mode='nearest',
            cval=0.0)

    def shift(img):
        """ shift image in range of [width_shift_range/height_shift_range]"""
        return image.random_shift(
            img,
            width_shift_range,
            height_shift_range,
            row_axis=0,
            col_axis=1,
            channel_axis=2,
            fill_mode='nearest',
            cval=0.0)

    def shear(img):
        """ random shear in range of [intensity], note intensity in angle """
        return image.random_shear(
            img,
            shear_range,
            row_axis=0,
            col_axis=1,
            channel_axis=2,
            fill_mode='nearest',
            cval=0.0)

    def zoom(img):
        """ random zoom image in width/height in [zoom_range] """
        return image.random_zoom(
            img,
            zoom_range,
            row_axis=0,
            col_axis=1,
            channel_axis=2,
            fill_mode='nearest',
            cval=0.0)

    def channel_shift(img):
        """ keras function deduct intensity directly, this is for compatibility """
        intensity = 255 * channel_shift_range
        return image.random_channel_shift(img, intensity, channel_axis=2)

    func = random.choice([rotate, shift, shear, zoom, channel_shift])
    return func(img)


def transform_pipeline(path):
    """ read img from path then random transform it """
    img = read_prepare_img(path, size=224)
    img = random_transform(img)
    return img


def taking_out(item, idx, batch_size):
    """ taking a [batch_size] items the [idx+1]th time """
    length = int(np.ceil(len(item) / float(batch_size)))
    if idx >= length:
        raise ValueError("Requested index is out of range")
    elif idx == length - 1:
        first_half = item[idx * batch_size:]
        second_half = item[:batch_size - len(first_half)]
        return first_half + second_half
    else:
        batch = item[idx * batch_size:(idx + 1) * batch_size]
        return batch


def shuffle_two_lists(list_0, list_1):
    """ helper function to synchronize shuffle """
    temp_list = list(zip(list_0, list_1))
    random.shuffle(temp_list)
    return zip(*temp_list)


class DataSequence(Sequence):
    """ Sequence container compatilbles with multi gpus"""

    def __init__(self,
                 x_list,
                 y_list,
                 workers=4,
                 batch_size=40,
                 no_transform=False):
        self.params = {
            'workers': workers,
            'batch_size': batch_size,
            'no_transform': no_transform
        }
        self.x_set = x_list
        self.y_set = y_list
        self.length = int(
            np.ceil(len(self.x_set) / float(self.params['batch_size'])))

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        if idx > self.__len__():
            raise IndexError("Illegal index")
        else:
            batch_x = taking_out(self.x_set, idx, self.params['batch_size'])
            batch_y = taking_out(self.y_set, idx, self.params['batch_size'])
            with mp.Pool(processes=self.params['workers']) as pool:
                if self.params['no_transform']:
                    batch_x_processed = pool.map(read_prepare_img, batch_x)
                else:
                    batch_x_processed = pool.map(transform_pipeline, batch_x)
        return np.array(batch_x_processed), np.array(batch_y)
