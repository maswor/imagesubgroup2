"""
Main training program
"""
import sys
import os
import itertools
import argparse
from pathlib import Path
from sklearn import preprocessing
import multiprocessing as mp
import tensorflow as tf

stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')
from keras import callbacks, utils, layers, models
from model import ResNet50, ResNet50MGPU
sys.stderr = stderr

from sequence import DataSequence, shuffle_two_lists


def parse_args(args):
    """ Add argument for selecting Data files """
    parser = argparse.ArgumentParser(
        description="Train a CNN classifier for Leaf deseases")
    parser.add_argument('dataPath', help="Path to Data folder")
    parser.add_argument(
        '--processNum',
        type=int,
        default=4,
        help="Number of concurrent processes")
    parser.add_argument(
        '--gpusNum', type=int, default=1, help="Number of GPUs available")
    return parser.parse_args(args)


def list_sub_dirs(path):
    """ list sub-directories in PosixPath format """

    return sorted([x for x in path.iterdir() if x.is_dir()])


def glob_files(path, extension=['.png']):
    """ find all file with extensions """
    final_list = []
    for ext in extension:
        temp_list = list(path.glob('*' + ext))
        final_list += temp_list
    return sorted(final_list)


def generate_xy_list(path):
    """
    Parse folder, take pictures and label them with their folder name
    input: path of pathlib.Path type
    """
    sub_dirs = list(list_sub_dirs(path))
    class_ids = [path.parts[-1] for path in sub_dirs]
    data_dir = [(path / 'output') for path in sub_dirs]
    # initilize Encode labels (from ['foo', 'bar', 'foobar'] to [0, 1, 2]
    label_encoder = preprocessing.LabelEncoder()
    label_encoder.fit(class_ids)
    x_lists = []
    y_lists = []
    for _dir, _idx in zip(data_dir, class_ids):
        files = glob_files(_dir, ['.png', '.jpg'])
        y_lists_per_folder = (_idx,) * len(files)
        x_lists_per_folder = tuple(
            [str(x.expanduser().resolve()) for x in files])
        # using the encoder
        y_lists_per_folder = label_encoder.transform(y_lists_per_folder)
        x_lists.append(x_lists_per_folder)
        y_lists.append(y_lists_per_folder)
    x_lists = list(itertools.chain.from_iterable(x_lists))
    y_lists = list(itertools.chain.from_iterable(y_lists))
    # From [0, 1, 2] to [100, 010, 001]
    y_lists = utils.to_categorical(y_lists)
    return x_lists, y_lists


class CNNModel(object):
    """ Parser folder to get file and its class """

    def __init__(self, path, workers=4, gpus=1):
        self.params = {
            'path': path,
            'workers': workers,
            'gpus': gpus,
        }
        self.train_sequence = None
        self.test_sequence = None
        self.model = None

    def create_sequence(self, batch_size=40, split=0.8):
        """ create Sequence container for storing traing data """
        path = Path(self.params['path'])
        x_list, y_list = generate_xy_list(path)
        x_list, y_list = shuffle_two_lists(x_list, y_list)
        limit = int(split * len(y_list))
        self.train_sequence = DataSequence(
            x_list[:limit],
            y_list[:limit],
            workers=self.params['workers'],
            batch_size=batch_size,
            no_transform=False)

        self.test_sequence = DataSequence(
            x_list[limit:],
            y_list[limit:],
            workers=self.params['workers'],
            batch_size=batch_size,
            no_transform=True)

    def create_model(self, numb_class=7):
        """ Create training model, for multi-gpus:
            create one for CPU and n for n GPUs
            For single gpu: create one for GPU """
        # To use other than (224, 224, 3) please edit sequen.py file as well
        base_model = ResNet50(
            include_top=False,
            weights='imagenet',
            pooling='avg',
            input_shape=(224, 224, 3))
        x = base_model.output
        x = layers.Dense(1024, activation='relu')(x)
        predictions = layers.Dense(numb_class, activation='softmax')(x)
        my_model = models.Model(inputs=base_model.input, outputs=predictions)
        for layer in my_model.layers:
            layer.trainable = True
        # callback to save best weights
        if self.params['gpus'] == 1:
            my_model.compile(
                optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy'])
            self.model = my_model

        else:
            parallel_model = ResNet50MGPU(my_model, self.params['gpus'])
            parallel_model.compile(
                optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy'])
            self.model = parallel_model

    def run_model(self, epochs=10):
        """ load weights (if any) and train the model """
        mod_wt_path = './7_plus_bg_best_weights.hdf5'
        best_wts_callback = callbacks.ModelCheckpoint(
            mod_wt_path, save_best_only=True, save_weights_only=True)

        if Path(mod_wt_path).is_file():
            print("load existing weights")
            self.model.load_weights(mod_wt_path)

        # compile and train
        self.model.fit_generator(
            self.train_sequence,
            epochs=epochs,
            callbacks=[best_wts_callback],
            validation_data=self.test_sequence,
            max_queue_size=10,
            workers=self.params['workers'],
            use_multiprocessing=False,
            shuffle=True)


def main(args=None):
    """ Main program, construct mult gpus and train here """
    # uncomment this line if you have multi gpus but want to use one
    # os.environ["CUDA_VISIBLE_DEVICES"] = "2"
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # Dynamic memory allocation
    sess = tf.Session(config=config)

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    process_numb = args.processNum
    gpu_numb = args.gpusNum
    data_path = args.dataPath

    my_cnn = CNNModel(data_path, process_numb, gpu_numb)
    my_cnn.create_sequence(batch_size=128, split=0.8)
    my_cnn.create_model(numb_class=8)
    my_cnn.run_model(epochs=1)


if __name__ == '__main__':
    mp.get_context('fork')
    sys.exit(main())
