1, Please install OpenCV from source with option for patented algorithm (which BM3D is)
2, Alternatively, our code has another code path for pybm3d, easier to install from Pip
3, We also use colorama to pretty print error
4, Other than that, it's very standard setup: Scikit-image, Scipy, Numpy, Python 2.7

Thank you for reading.
