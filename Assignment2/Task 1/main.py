""" python main.py -f ../Data/Data2/image/image pairs """
from __future__ import print_function
import argparse
import sys
import matplotlib.pyplot as plt
from ImageEnhance import ImageEnhance
import cv2
from colorama import init


def parse_args():
    """ fts """
    parser = argparse.ArgumentParser(
        description="Enhancing corrupted images and compared them to originals")
    parser.add_argument('-f', '--folder', help='Image Pairs folder',
                        type=str, required=True)
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    init(autoreset=True)
    try:
        my_img_enhance = ImageEnhance(args.folder)
    except ValueError:
        print("I don't think the folder exists")
    my_img_enhance.save_statistics()
    my_img_enhance.plot_t_sne(2)
