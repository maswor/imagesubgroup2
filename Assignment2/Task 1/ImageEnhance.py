""" Class which stores and process the data """
from __future__ import print_function
import os
import glob
import cv2
from datetime import datetime
import numpy as np
from skimage import io, exposure, img_as_float, img_as_ubyte
from scipy.stats import wasserstein_distance
from colorama import Fore, Back, Style
from skimage.measure import compare_ssim, compare_psnr
from sklearn.manifold import TSNE
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


class ImageEnhance(object):
    """ Class which store images and process on them """

    def __init__(self, root_dir):
        self.root_dir = root_dir
        self.processed_img = [[] for _ in range(4)]
        self.processed_row = {'HIST': 0, 'CLAHE': 1, 'GAMMA': 2, 'BM3D': 3}
        self.tsne_array = np.zeros((96, 3))
        self.tsne_array_2 = np.zeros((96, 2))
        self.tsne_row = {'ORI': 0, 'CRPT': 1, 'HIST': 2,
                         'CLAHE': 3, 'GAMMA': 4, 'BM3D': 5}

        self.metrics = [[] for _ in range(3)]
        self.metrics_row = {'PSNR': 0, 'SSIM': 1, 'EMD': 2}
        # Check user inputed path then create imgs list
        if not os.path.exists(root_dir):
            raise ValueError
        self.corrupted_img_paths = glob.glob(
            os.path.join(root_dir, '*_d25.png'))
        # Create list of corrupted images
        self.ori_img_paths = []
        for file_path in self.corrupted_img_paths:
            file_path = self.corrupted_img_paths[0][:-
                                                    8] + self.corrupted_img_paths[0][-4:]
            self.ori_img_paths.append(file_path)

    def denoise(self, method):
        """ denoise all images using 1 of 4 method """
        print("Start denoising using {} method".format(method))
        start_time = datetime.now()
        row = self.processed_row[method]
        # HIST
        if row == 0:
            for img_path in self.corrupted_img_paths:
                img = cv2.imread(img_path, 0)
                image_hist = cv2.equalizeHist(img)
                self.processed_img[row].append(image_hist)
        # CLAHE
        elif row == 1:
            for img_path in self.corrupted_img_paths:
                img = cv2.imread(img_path, 0)
                clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
                cl1 = clahe.apply(img)
                self.processed_img[row].append(cl1)
        # GAMMA
        elif row == 2:
            for img_path in self.corrupted_img_paths:
                img = io.imread(img_path, as_grey=True)
                img_float = img_as_float(img)
                img_float = exposure.adjust_gamma(img_float, 0.4)
                img = img_as_ubyte(img_float)
                self.processed_img[row].append(img)
        # BM3D
        elif row == 3:
            def bm3d(img):
                """ 2 code paths """
                try:
                    out_img = cv2.xphoto.bm3dDenoising(img)
                except Exception:
                    print(
                        Fore.RED + 'You didn\'t config OpenCV with Non-free algorithm, using Pybm3d instead, which is very show')
                    try:
                        import pybm3d
                    except ImportError:
                        print(Fore.RED + 'You don\'t have pybm3d installed either')
                    out_img = pybm3d.bm3d.bm3d(img, 40)
                return out_img

            for img_path in self.corrupted_img_paths:
                img = cv2.imread(img_path, 0)
                out_img = bm3d(img)
                self.processed_img[row].append(out_img)
        total = (datetime.now() - start_time).total_seconds()
        print("Finish denoising using {} method in {} seconds".format(
            method, total))

    def compute_metric(self, metric, method):
        """ Compute PSNR, SSIM and EMD """
        row = self.processed_row[method]
        m_row = self.metrics_row[metric]
        psnr = 0
        ssim = 0
        emd = 0
        length = len(self.processed_img[row])
        # PSNR
        if m_row == 0:
            for (i, img) in enumerate(self.processed_img[row]):
                ori_img = cv2.imread(self.ori_img_paths[i], 0)
                psnr += compare_psnr(
                    ori_img, img, data_range=img.max() - img.min())
            self.metrics[m_row].append(psnr / length)
        # SSIM
        if m_row == 1:
            for (i, img) in enumerate(self.processed_img[row]):
                ori_img = cv2.imread(self.ori_img_paths[i], 0)
                ssim += compare_ssim(
                    ori_img, img, data_range=img.max() - img.min())
            self.metrics[m_row].append(ssim / length)
        # EMD
        if m_row == 2:
            for (i, img) in enumerate(self.processed_img[row]):
                hist = cv2.calcHist([img], [0], None, [256], [0, 256])
                ori_img = cv2.imread(self.ori_img_paths[i], 0)
                hist_ori = cv2.calcHist([ori_img], [0], None, [256], [0, 256])
                emd += wasserstein_distance(hist_ori[:, 0], hist[:, 0])
            self.metrics[m_row].append(emd / length)

    def t_sne(self, dim):
        """ Calculate T-SNE, assume image size 512 * 512 """
        print("Start calculating t-distributed stochastic neighbor embedding")
        start_time = datetime.now()
        tsne = TSNE(n_components=dim, verbose=1)
        my_big_data = self.processed_img  # 4 * 16 * 512 * 512
        ori_imgs = [[cv2.imread(img_path, 0)
                     for img_path in self.ori_img_paths]]  # 1 * 16 * 512 * 512
        crpt_imgs = [[cv2.imread(img_path, 0)
                      for img_path in self.corrupted_img_paths]]
        my_big_data = np.concatenate(
            (my_big_data, ori_imgs, crpt_imgs), axis=0)
        my_big_data = np.reshape(my_big_data, (-1, 262144))
        if dim == 2:
            self.tsne_array_2 = tsne.fit_transform(my_big_data)
        else:
            self.tsne_array = tsne.fit_transform(my_big_data)
        total = (datetime.now() - start_time).total_seconds()
        print("Finish calculating t-sne in {} seconds".format(total))

    def save_statistics(self):
        """ save 3 * 6 * 16 metrics """
        print('Saving statistics to file')
        for (method, idx) in self.processed_row.items():
            self.denoise(method)
            for i, img in enumerate(self.processed_img[idx]):
                cv2.imwrite('./Output/{}/{}.jpg'.format(method, i), img)
            for (metric, idx2) in self.metrics_row.items():
                self.compute_metric(metric, method)
                np.savetxt('./Output/stat{}.csv'.format(metric),
                           self.metrics[idx2])
        print('Done saving statistics!')

    def plot_t_sne(self, dim):
        if dim == 2:
            self.t_sne(2)
            fig = plt.figure(figsize=(10, 10), dpi=200)
            X = self.tsne_array_2[:, 0]
            Y = self.tsne_array_2[:, 1]
            C = np.zeros_like(self.tsne_array_2[:, 0])
            for (i, item) in enumerate(C):
                C[i] = i % 16
            plt.scatter(X, Y, c=C)
        elif dim == 3:
            self.t_sne(3)
            fig = plt.figure(figsize=(10, 10), dpi=200)
            ax = fig.add_subplot(111, projection='3d')
            X = self.tsne_array[:, 0]
            Y = self.tsne_array[:, 1]
            Z = self.tsne_array[:, 2]
            C = np.zeros_like(self.tsne_array[:, 0])
            for (i, item) in enumerate(C):
                C[i] = i % 16
            ax.scatter(X, Y, Z, c=C)

        plt.colorbar()
        print('Saving plot...')
        plt.savefig("./Output/tsne.png")
        print('Done')
