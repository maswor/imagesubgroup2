import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('1.jpg',0)
edges = cv2.Canny(img,80,10,apertureSize = 3) # canny Edge OR

# Hough's Probabilistic Line Transform 
minLineLength = 2000
maxLineGap = 5
lines = cv2.HoughLinesP(edges,1,np.pi/2,50,minLineLength,maxLineGap)

for i in range(len(lines)):
    line = lines[i]
    for x1,y1,x2,y2 in line:
        if x1==x2:
            lines[i][0][0] = 0
            lines[i][0][2] = 0

print(lines)
for line in lines:
    for x1,y1,x2,y2 in line:
        cv2.line(img,(x1,y1),(x2,y2),(15,15,15),2)

cv2.imwrite('iteration_1.jpg',img)


img = cv2.imread('iteration_1.jpg',0)
edges = cv2.Canny(img,80,10,apertureSize = 3) # canny Edge OR

# Hough's Probabilistic Line Transform 
minLineLength = 2000
maxLineGap = 40
lines = cv2.HoughLinesP(edges,1,np.pi/2,50,minLineLength,maxLineGap)

for i in range(len(lines)):
    line = lines[i]
    for x1,y1,x2,y2 in line:
        if y1==y2:
            lines[i][0][1] = 0
            lines[i][0][3] = 0

print(lines)
for line in lines:
    for x1,y1,x2,y2 in line:
        cv2.line(img,(x1,y1),(x2,y2),(15,15,15),2)

cv2.imwrite('iteration_2.jpg',img)


img = cv2.imread('iteration_2.jpg',0)
edges = cv2.Canny(img,80,10,apertureSize = 3) # canny Edge OR

# Hough's Probabilistic Line Transform 
minLineLength = 2000
maxLineGap = 5
lines = cv2.HoughLinesP(edges,1,np.pi/2,50,minLineLength,maxLineGap)

for i in range(len(lines)):
    line = lines[i]
    for x1,y1,x2,y2 in line:
        if x1==x2:
            lines[i][0][0] = 0
            lines[i][0][2] = 0

print(lines)
for line in lines:
    for x1,y1,x2,y2 in line:
        cv2.line(img,(x1,y1),(x2,y2),(15,15,15),2)

cv2.imwrite('iteration_3.jpg',img)



img = cv2.imread('2.jpg',0)
edges = cv2.Canny(img,80,10,apertureSize = 3) # canny Edge OR

# Hough's Probabilistic Line Transform 
minLineLength = 2000
maxLineGap = 30
lines = cv2.HoughLinesP(edges,1,np.pi/2,50,minLineLength,maxLineGap)

for i in range(len(lines)):
    line = lines[i]
    for x1,y1,x2,y2 in line:
        if x1==x2:
            lines[i][0][0] = 0
            lines[i][0][2] = 0

print(lines)
for line in lines:
    for x1,y1,x2,y2 in line:
        cv2.line(img,(x1,y1),(x2,y2),(55,55,55),2)

cv2.imwrite('2iteration_1.jpg',img)



img = cv2.imread('2iteration_1.jpg',0)
edges = cv2.Canny(img,80,10,apertureSize = 3) # canny Edge OR

# Hough's Probabilistic Line Transform 
minLineLength = 800
maxLineGap = 15
lines = cv2.HoughLinesP(edges,1,np.pi/2,50,minLineLength,maxLineGap)

for i in range(len(lines)):
    line = lines[i]
    for x1,y1,x2,y2 in line:
        if y1==y2:
            lines[i][0][1] = 0
            lines[i][0][3] = 0

print(lines)
for line in lines:
    for x1,y1,x2,y2 in line:
        cv2.line(img,(x1,y1),(x2,y2),(55,55,55),2)

cv2.imwrite('2iteration_2.jpg',img)



img = cv2.imread('2iteration_2.jpg',0)
edges = cv2.Canny(img,80,10,apertureSize = 3) # canny Edge OR

# Hough's Probabilistic Line Transform 
minLineLength = 2000
maxLineGap = 15
lines = cv2.HoughLinesP(edges,1,np.pi/2,50,minLineLength,maxLineGap)

for i in range(len(lines)):
    line = lines[i]
    for x1,y1,x2,y2 in line:
        if x1==x2:
            lines[i][0][0] = 0
            lines[i][0][2] = 0

print(lines)
for line in lines:
    for x1,y1,x2,y2 in line:
        cv2.line(img,(x1,y1),(x2,y2),(55,55,55),2)

cv2.imwrite('2iteration_3.jpg',img)


img = cv2.imread('iteration_3.jpg',0)

# thresholding
ret, thresh = cv2.threshold(img,18,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
for counter, line in enumerate(thresh):
    for counter2, num in enumerate(line):
        if num == 255:
            img[counter][counter2] = 0

plt.imshow(thresh,'gray')
plt.show()
cv2.imwrite('labels_removed_1.jpg',img)

img = cv2.imread('labels_removed_1.jpg',0)

# thresholding
ret, thresh = cv2.threshold(img,30,200,cv2.THRESH_BINARY)

for counter, line in enumerate(thresh):
    for counter2, num in enumerate(line):
        if num == 0:
            img[counter][counter2] = 0

plt.imshow(thresh,'gray')
plt.show()
cv2.imwrite('num_1_background_removed.jpg',img)


img = cv2.imread('2iteration_3.jpg',0)

# thresholding
ret, thresh = cv2.threshold(img,150,255,cv2.THRESH_BINARY)
for counter, line in enumerate(thresh):
    for counter2, num in enumerate(line):
        if num == 255:
            img[counter][counter2] = 0

plt.imshow(thresh,'gray')
plt.show()

cv2.imwrite('2labels_removed.jpg',img)


img = cv2.imread('2labels_removed.jpg',0)

# thresholding
ret, thresh = cv2.threshold(img,70,255,cv2.THRESH_BINARY)
for counter, line in enumerate(thresh):
    for counter2, num in enumerate(line):
        if num == 0:
            img[counter][counter2] = 0

plt.imshow(thresh,'gray')
plt.show()

cv2.imwrite('num_2_background_removed.jpg',img)