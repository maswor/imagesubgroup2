from pathlib import Path, PosixPath
import pytest
from ..step1 import list_sub_dirs, glob_files, DatasetCreator, apply_clahe, cal_hist_on_hsv, threshold_in_hsv, cal_descriptors, cal_hist_and_descriptor_from_path
import cv2


@pytest.fixture(scope="session", autouse=False)
def setup():
    img = cv2.imread('./tests/testData/1/output/1.png')
    img = apply_clahe(img)
    img_hsv = threshold_in_hsv(img, [0, 100, 0, 256, 50, 255])

    yield img_hsv


class TestClass(object):

    def test_list_sub_dirs(self):
        assert list_sub_dirs(
            Path('./tests/testData')) == [PosixPath('tests/testData/1'), PosixPath('tests/testData/2')]

    def test_glob_files(self):
        assert sorted(glob_files(Path('./tests/testData/1/output'),
                                 '.png')) == sorted([PosixPath('tests/testData/1/output/1.png'), PosixPath('tests/testData/1/output/2.png')])

    def test_apply_clahe(self, setup):
        img = setup
        assert img.shape == (720, 720, 3)

    def test_cal_hist_on_hsv(self, setup):
        img = setup
        hist = cal_hist_on_hsv(img)
        print(hist)
        assert len(hist[0]) == 256

    def test_cal_descriptors(self, setup):
        img = setup
        descs = cal_descriptors(img)
        assert len(descs[0]) == 64

    def test_cal_hist_and_descriptor_from_path(self):
        hist, descs = cal_hist_and_descriptor_from_path(
            './tests/testData/1/output/1.png')
        assert len(hist[0]) == 256 and len(descs[0]) == 64

    def test_DatasetGenerator(self):
        datagen = DatasetCreator('./tests/testData')
        datagen.generate_train_test_list()
        a, b = datagen.random_shuffle()
        datagen.run()
        assert len(a) / len(b) == 0.8/0.4
