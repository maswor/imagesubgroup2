"""
Please enter: Python3 demo.py model kmean img_path --processNum 4 --cache_size 1000
Note: We left the model and kmean files in this same folder
"""
import argparse
import pickle
import sys
import numpy as np
from sklearn import svm, model_selection, metrics, externals


def parse_args(args):
    """ Add argument for selecting Data files """
    parser = argparse.ArgumentParser(
        description="Train a SVM classifier for Leaf deseases")
    parser.add_argument('path_file', help="pickle file with paths to imgs")
    parser.add_argument('BOF_file', help="pickle file with features")
    parser.add_argument('--processNum', type=int, default=4,
                        help="Number of concurrent processes")
    parser.add_argument('--cache_size', type=int, default=500,
                        help="Cache for kernel (in MB)")
    return parser.parse_args(args)


def open_pickle(path):
    """ helper function to open file and close it nicely """
    with open(path, 'rb') as file:
        _file = pickle.load(file)
        return _file


def save_pickle(name, data):
    """ helper function to save file and close it nicely """
    with open(name, 'wb') as file:
        pickle.dump(data, file, pickle.HIGHEST_PROTOCOL)


class Classification(object):
    """ Class for implementing SVM training """

    def __init__(self, path_file, BOF_file, n_jobs):
        self.path_file = path_file
        self.BOF_file = BOF_file
        self.n_jobs = n_jobs
        self.X = None
        self.Y = None
        self.my_grid = None

    def prepare_data(self):
        """ Prepare X, Y data to feed to SVM """
        print('Loading BOF file')
        BOF = open_pickle(self.BOF_file)
        self.X = np.concatenate((BOF[0], BOF[1]), axis=1)
        print('Loading ground truth')
        path = open_pickle(self.path_file)
        self.Y = np.array([p[1] for p in path])

    def train(self, tunned_parameters, fold=5, cache_size=500):
        """ Train the model on BOF and labels """
        clf = svm.SVC(cache_size=cache_size)
        print('Calculate score..')
        self.my_grid = model_selection.GridSearchCV(
            estimator=clf, param_grid=tunned_parameters, scoring='accuracy', cv=fold, n_jobs=self.n_jobs)
        self.my_grid.fit(self.X, self.Y)
        print("Best parameters set found on the development set: \n")
        print(self.my_grid.best_params_, "\n")
        print("Grid scores on development set:\n")
        means = self.my_grid.cv_results_['mean_test_score']
        stds = self.my_grid.cv_results_['std_test_score']
        params = self.my_grid.cv_results_['params']
        for mean, std, param in zip(means, stds, params):
            print("{} (+/-{}) for {}".format(mean, std, param))
        Y_prd = self.my_grid.predict(self.X)
        print(metrics.classification_report(self.Y, Y_prd))

    def save_model(self, name='Model.pkl'):
        """ Save best trained model for later usesage """
        print("Saving the model..")
        externals.joblib.dump(self.my_grid.best_estimator_, name)


def main(args=None):
    """ Main program, required to be a function for multiprocess """
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    PROCESSES = args.processNum
    path_file = args.path_file
    BOF_file = args.BOF_file
    cache_size = args.cache_size

    cls = Classification(path_file, BOF_file, PROCESSES)
    cls.prepare_data()
    # Parameters you can choose from: kernel (poly, linear, brf), C, gamma, degree..etc.
    tunned_parameters = [{'kernel': ['rbf'], 'C': [10], 'gamma':[1]}]
    cls.train(tunned_parameters, fold=5, cache_size=cache_size)
    cls.save_model()
    # cls.save_model()


if __name__ == "__main__":
    sys.exit(main())
