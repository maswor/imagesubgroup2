"""
Please enter: Python3 step1.py /path/to/data path_file features_file --processNum = 4
Note: path_file and features_file are resuted from step1
"""
import argparse
import sys
import pickle
from sklearn import cluster, preprocessing, externals
from collections import Counter
import numpy as np


def parse_args(args):
    """ Add argument for selecting Data files """
    parser = argparse.ArgumentParser(
        description="Train a SVM classifier for Leaf deseases")
    parser.add_argument('path_file', help="pickle file with paths to imgs")
    parser.add_argument('features_file', help="pickle file with features")
    parser.add_argument('--processNum', type=int, default=4,
                        help="Number of concurrent processes")
    return parser.parse_args(args)


def open_pickle(path):
    """ helper function to open file and close it nicely """
    with open(path, 'rb') as file:
        _file = pickle.load(file)
        return _file


def save_pickle(name, data):
    """ helper function to save file and close it nicely """
    with open(name, 'wb') as file:
        pickle.dump(data, file, pickle.HIGHEST_PROTOCOL)


def create_kmeans_cluster(centroid_num=4, process_num=4):
    """ Create a Kmean object, which you can apply to data """
    kmeans = cluster.MiniBatchKMeans(
        n_clusters=256, max_iter=100, batch_size=100, verbose=True, compute_labels=False, max_no_improvement=10)
    return kmeans


def extract_features(features):
    """
    features = [[[hist], [[surf], [surf]..[surf]]]*n]
    This will extract surf
    """
    n_features = []
    for item in features:
        for m_feature in item[1]:
            n_features.append(m_feature)

    return n_features


def extract_hist(features):
    """
    features = [[[hist], [[surf], [surf]..[surf]]]*n]
    This will extract surf
    """
    n_hist = []
    for item in features:
        n_hist.append(item[0])
    return np.array(n_hist).reshape(-1, 256)


def count_and_normalize(labels, maximum=256):
    """ given a vector, count and normalize a histogram in range [0, maximum)] """
    cnt = Counter(labels)
    result = np.array([cnt[i] for i in range(maximum)])
    result = preprocessing.normalize(result.reshape(-1, 256))
    return result


class BagOfFeatures(object):
    """ Class to find 256 centroid, then assign each point to centroid """

    def __init__(self, features_file_path, centroid_num=256, process_num=4):
        self.centroid_num = centroid_num
        self.process_num = process_num
        self.features_file_path = features_file_path
        self.features = None
        self.m_kmeans = None
        self.BOF = None
        self.hist_features = None

    def create_features_bag(self):
        """
        Create a kMeans object, train with entire features set.
        Find 256 (defaut value) centroids as bag of features
        """
        print('Loading data')
        self.features = open_pickle(self.features_file_path)
        print('Finish loading data. Start training')
        self.m_kmeans = create_kmeans_cluster(
            centroid_num=self.centroid_num, process_num=self.process_num)
        surf_features = extract_features(self.features)
        self.m_kmeans.fit(surf_features)
        print('Training finish')

    def quantize_vector(self):
        """
        traverse the features list, assign each SURF features to a features.
        count and make it into a histogram. Normalize the histogram
        """
        print("Quantizing BOF")
        self.hist_features = extract_hist(self.features)
        labels = []
        for img in self.features:
            label = self.m_kmeans.predict(img[1])
            label = count_and_normalize(label)
            labels.append(label)
        self.BOF = np.array(labels).reshape(-1, 256)
        print("Finish quantizing BOF")

    def save_features(self):
        save_pickle('BOF.pickle', np.array([self.hist_features, self.BOF]))

    def save_k_means(self, name):
        externals.joblib.dump(self.m_kmeans, name)


def main(args=None):
    """ Main program, required to be a function for multiprocess """
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    PROCESSES = args.processNum
    path_file = args.path_file
    features_file_path = args.features_file

    BOF = BagOfFeatures(features_file_path,
                        centroid_num=256, process_num=PROCESSES)
    BOF.create_features_bag()
    BOF.quantize_vector()
    BOF.save_features()
    BOF.save_k_means('kmean.mkl')


if __name__ == "__main__":
    sys.exit(main())
