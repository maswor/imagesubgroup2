"""
Please enter: Python3 demo.py model.pkl image
Note: model.pickle is saved model, image is your provided image
"""
import argparse
from collections import Counter
import sys
import cv2
import numpy as np
from sklearn import preprocessing, externals


def parse_args(args):
    """ Add argument for selecting Data files """
    parser = argparse.ArgumentParser(
        description="Train a SVM classifier for Leaf deseases")
    parser.add_argument('model', help="pkl file of trained model")
    parser.add_argument('kmeans', help="pkl file of kmeans model")
    parser.add_argument('img', help="your provided img")
    return parser.parse_args(args)


def apply_clahe(img, each_size=720):
    """ Clahe used to normalize the lighting on BGR images"""
    # First resize and add padding to make (720, 720)
    old_size = img.shape[:2]
    ratio = each_size/max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])
    img = cv2.resize(img, dsize=(
        new_size[1], new_size[0]), interpolation=cv2.INTER_AREA)
    pad_vert_each = (each_size - new_size[0]) // 2
    pad_hori_each = (each_size - new_size[1]) // 2
    img = cv2.copyMakeBorder(img, pad_vert_each, pad_vert_each,
                             pad_hori_each, pad_hori_each, cv2.BORDER_CONSTANT, value=[0, 0, 0])
    # Second, convert to Lab and do CLAHE and light channel
    img = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    img_planes = cv2.split(img)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    img = clahe.apply(img_planes[0])
    img = cv2.merge(img_planes)
    img = cv2.cvtColor(img, cv2.COLOR_LAB2BGR)

    return img


def threshold_in_hsv(img, hsv_range_list):
    """Threshold to remove background, In: BGR our: HSV image.
    From trial and error: [0, 100, 0, 255, 50, 255]"""
    min_h, max_h, min_s, max_s, min_v, max_v = hsv_range_list
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lower_color = np.array([min_h, min_s, min_v])
    upper_color = np.array([max_h, max_s, max_v])
    mask = cv2.inRange(img_hsv, lower_color, upper_color)
    img_hsv = cv2.bitwise_and(img_hsv, img_hsv, mask=mask)

    return img_hsv


def cal_hist_on_hsv(img_hsv):
    """ Calculate histogram on grayscale, input: HSV """
    bgr = cv2.cvtColor(img_hsv, cv2.COLOR_HSV2BGR)
    gray = cv2.cvtColor(bgr, cv2.COLOR_BGR2GRAY)
    hist = cv2.calcHist([gray], [0], None, [256], [0, 256])
    hist = hist.reshape(1, -1)
    hist = preprocessing.normalize(hist, norm='l2')

    return hist


def cal_descriptors(img_hsv):
    """ calculate descriptors for one hsv image """
    # TODO: parallel this shit
    img_rgb = cv2.cvtColor(img_hsv, cv2.COLOR_HSV2RGB)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
    surf = cv2.xfeatures2d_SURF.create(400)
    _, descs = surf.detectAndCompute(img_gray, None)
    return descs


def cal_hist_and_descriptor_from_path(path):
    """ a huge funtion for whole pipeline so that it can be excuted in paralell """
    hsv_range_list = [0, 100, 0, 256, 50, 255]
    img = cv2.imread(path)
    img = apply_clahe(img)
    img_hsv = threshold_in_hsv(img, hsv_range_list)
    hist = cal_hist_on_hsv(img_hsv)
    descs = cal_descriptors(img_hsv)

    return hist, descs

# From step 2


def extract_features(features):
    """
    features = [[[hist], [[surf], [surf]..[surf]]]*n]
    This will extract surf
    """
    n_features = []
    for item in features:
        for m_feature in item[1]:
            n_features.append(m_feature)

    return n_features


def extract_hist(features):
    """
    features = [[[hist], [[surf], [surf]..[surf]]]*n]
    This will extract surf
    """
    n_hist = []
    for item in features:
        n_hist.append(item[0])
    return np.array(n_hist).reshape(-1, 256)


def count_and_normalize(labels, maximum=256):
    """ given a vector, count and normalize a histogram in range [0, maximum)] """
    cnt = Counter(labels)
    result = np.array([cnt[i] for i in range(maximum)])
    result = preprocessing.normalize(result.reshape(-1, 256))
    return result


class Predictor(object):
    def __init__(self, img_path, model_path, kmeans_path):
        self.img_path = img_path
        self.model_path = model_path
        self.kmeans_path = kmeans_path
        self.model = None
        self.kmeans = None
        self.features = None

    def load_data(self):
        self.features = cal_hist_and_descriptor_from_path(self.img_path)
        self.features = np.expand_dims(self.features, axis=0)
        self.model = externals.joblib.load(self.model_path)
        self.kmeans = externals.joblib.load(self.kmeans_path)

    def predict(self):
        label = self.kmeans.predict(self.features[0, 1])
        label = count_and_normalize(label)
        hist = self.features[0, 0]
        X = np.concatenate((hist, label), axis=1)
        Y_prd = self.model.predict(X)
        print("We predict the image has class number: {}".format(Y_prd[0]))


def main(args=None):
    """ Main program, required to be a function for multiprocess """
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    img_path = args.img
    model_path = args.model
    kmeans_path = args.kmeans

    pdt = Predictor(img_path, model_path, kmeans_path)
    pdt.load_data()
    pdt.predict()


if __name__ == "__main__":
    sys.exit(main())
