"""
Please enter: Python3 step1.py /path/to/data --processNum = 4
Note: Each worker will spawn it own thresh, 4 will satuarate all your 8 logical cores
"""
import argparse
import sys
import multiprocessing as mp
from multiprocessing import Pool
import pickle
from pathlib import Path, PosixPath
from typing import List
from random import sample
import cv2
import numpy as np
from sklearn import preprocessing


def parse_args(args):
    """ Add argument for selecting Data files """
    parser = argparse.ArgumentParser(
        description="Train a SVM classifier for Leaf deseases")
    parser.add_argument('dataPath', help="Path to Data folder")
    parser.add_argument('--processNum', type=int, default=4,
                        help="Number of concurrent processes")
    return parser.parse_args(args)


def list_sub_dirs(path: Path) -> List[PosixPath]:
    """ list sub-directories in PosixPath format """

    return sorted([x for x in path.iterdir() if x.is_dir()])


def glob_files(path: Path, extension='.png') -> List[PosixPath]:
    """ find all file with extensions """

    return sorted(list(path.glob('*' + extension)))


def apply_clahe(img, each_size=720):
    """ Clahe used to normalize the lighting on BGR images"""
    # First resize and add padding to make (720, 720)
    old_size = img.shape[:2]
    ratio = each_size/max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])
    img = cv2.resize(img, dsize=(
        new_size[1], new_size[0]), interpolation=cv2.INTER_AREA)
    pad_vert_each = (each_size - new_size[0]) // 2
    pad_hori_each = (each_size - new_size[1]) // 2
    img = cv2.copyMakeBorder(img, pad_vert_each, pad_vert_each,
                             pad_hori_each, pad_hori_each, cv2.BORDER_CONSTANT, value=[0, 0, 0])
    # Second, convert to Lab and do CLAHE and light channel
    img = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    img_planes = cv2.split(img)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    img = clahe.apply(img_planes[0])
    img = cv2.merge(img_planes)
    img = cv2.cvtColor(img, cv2.COLOR_LAB2BGR)

    return img


def threshold_in_hsv(img, hsv_range_list):
    """Threshold to remove background, In: BGR our: HSV image.
    From trial and error: [0, 100, 0, 255, 50, 255]"""
    min_h, max_h, min_s, max_s, min_v, max_v = hsv_range_list
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lower_color = np.array([min_h, min_s, min_v])
    upper_color = np.array([max_h, max_s, max_v])
    mask = cv2.inRange(img_hsv, lower_color, upper_color)
    img_hsv = cv2.bitwise_and(img_hsv, img_hsv, mask=mask)

    return img_hsv


def cal_hist_on_hsv(img_hsv):
    """ Calculate histogram on grayscale, input: HSV """
    bgr = cv2.cvtColor(img_hsv, cv2.COLOR_HSV2BGR)
    gray = cv2.cvtColor(bgr, cv2.COLOR_BGR2GRAY)
    hist = cv2.calcHist([gray], [0], None, [256], [0, 256])
    hist = hist.reshape(1, -1)
    hist = preprocessing.normalize(hist, norm='l2')

    return hist


def cal_descriptors(img_hsv):
    """ calculate descriptors for one hsv image """
    # TODO: parallel this shit
    img_rgb = cv2.cvtColor(img_hsv, cv2.COLOR_HSV2RGB)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
    surf = cv2.xfeatures2d_SURF.create(400)
    _, descs = surf.detectAndCompute(img_gray, None)
    return descs


def cal_hist_and_descriptor_from_path(path):
    """ a huge funtion for whole pipeline so that it can be excuted in paralell """
    hsv_range_list = [0, 100, 0, 256, 50, 255]
    img = cv2.imread(path)
    img = apply_clahe(img)
    img_hsv = threshold_in_hsv(img, hsv_range_list)
    hist = cal_hist_on_hsv(img_hsv)
    descs = cal_descriptors(img_hsv)

    return hist, descs


class DatasetCreator(object):
    """ Generate a Train/Test list from given path """

    def __init__(self, path: str, workers=4, split=0.8):
        self.path = Path(path)
        self.workers = workers
        self.split = split
        # self.x_y_lists=[(PosixPath, 'class'], (PosixPath, 'class')]
        self.x_y_lists = []
        self.result = []

    def generate_train_test_list(self):
        """Parse folder, take pictures and label them with their folder name """
        sub_dirs = list(list_sub_dirs(self.path))
        class_ids = [path.parts[-1] for path in sub_dirs]
        data_dir = [(path / 'output') for path in sub_dirs]
        for _dir, _idx in zip(data_dir, class_ids):
            files = glob_files(_dir, '.png')
            y_list = (_idx,) * len(files)
            for x, y in zip(files, y_list):
                path = str(x.expanduser().resolve())
                self.x_y_lists.append([path, y])

    def random_shuffle(self):
        """ return train/test sets with ratio """
        total_numb = len(self.x_y_lists)
        x_y_shuffled = sample(self.x_y_lists, total_numb)
        x_y_train = x_y_shuffled[:int(self.split * total_numb)]
        x_y_test = x_y_shuffled[int(self.split * total_numb):]

        return x_y_train, x_y_test

    def run(self):
        all_paths = [(item[0],) for item in self.x_y_lists]
        ctx = mp.get_context('spawn')
        with ctx.Pool(processes=self.workers) as pool:
            self.result = pool.starmap(
                cal_hist_and_descriptor_from_path, all_paths)

    def save(self):
        with open('path.pickle', 'wb') as f:
            pickle.dump(self.x_y_lists, f, pickle.HIGHEST_PROTOCOL)
        with open('features.pickle', 'wb') as f:
            pickle.dump(self.result, f, pickle.HIGHEST_PROTOCOL)


def main(args=None):
    """ Main program, required to be a function for multiprocess """
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    PROCESSES = args.processNum
    path = args.dataPath
    datagen = DatasetCreator(path, workers=PROCESSES)
    datagen.generate_train_test_list()
    datagen.run()
    datagen.save()


if __name__ == "__main__":
    sys.exit(main())
