Note: 
1, All 3 steps files (especially spep1.py) use Multiprocessing lib extensively and could potential crash your pc. Reduce Process from 4 to 2 if needed.

2, Step 1: used for preprocessing, namely we threshold image to remove background, then We create a Histogram from the HSV image. Second, we create a bag of features using SURF features ( 1400 images * 1000 features * 64 bit).

3, Step 2: From SURF features, we use KMeans to create a codebook + Vector quantization to create a "histogram like" feature. We almost crash a 512GB-ram-workstation with that :).

3. Step 3 is a tool for testing different parameters for training models. Parameters are stored in a dict which allow mixed BRF/Poly/Linear kernel using a single function.

4. Demo is used for showing how the pipeline could be used for predict image score directly. Note: It's highly overfited for the trained data (ie. 99% accuracy, though for unseen data using Kfold with K = 5, it drops to 73%) 
