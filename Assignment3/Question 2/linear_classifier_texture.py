# USAGE
# python linear_classifier.py --dataset kaggle_dogs_vs_cats

# import the necessary packages
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import LinearSVC
from sklearn.metrics import classification_report
from sklearn.cross_validation import train_test_split
from imutils import paths
from skimage.feature import greycomatrix, greycoprops
from skimage import data
import numpy as np
import argparse
import imutils
import cv2
import os
import csv


def extract_color_histogram(image, bins=(8, 8, 8)):
	# extract a 3D color histogram from the HSV color space using
	# the supplied number of `bins` per channel
	hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
	hist = cv2.calcHist([hsv], [0, 1, 2], None, bins,
		[0, 180, 0, 256, 0, 256])

	# handle normalizing the histogram if we are using OpenCV 2.4.X
	if imutils.is_cv2():
		hist = cv2.normalize(hist)

	# otherwise, perform "in place" normalization in OpenCV 3 (I
	# personally hate the way this is done
	else:
		cv2.normalize(hist, hist)

	# return the flattened histogram as the feature vector
	return hist.flatten()

def extract_glcm(image):
    img = image[:,:,2]
    #img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img_length = (len(img))
    img_size = img.size
    img_width = int(img_size/img_length)
    PATCH_SIZE = 73

    grass_locations = [(0, 0)]
    grass_patches = []
    for loc in grass_locations:
        grass_patches.append(img[loc[0]:loc[0] + img_length,
                                loc[1]:loc[1] + img_width])

    # select some patches from sky areas of the image
    sky_locations = [(int(img_length/3), int(img_width/2)), (int(img_length/3*2), int(img_width/2))]
    sky_patches = []
    for loc in sky_locations:
        sky_patches.append(img[loc[0]:loc[0] + PATCH_SIZE,
                                loc[1]:loc[1] + PATCH_SIZE])

    img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img_length = (len(img))
    img_size = img.size
    img_width = int(img_size/img_length)
    PATCH_SIZE = 73

    rgrass_locations = [(0, 0)]
    rgrass_patches = []
    for loc in rgrass_locations:
        rgrass_patches.append(img[loc[0]:loc[0] + img_length,
                                loc[1]:loc[1] + img_width])

    # select some patches from sky areas of the image
    rsky_locations = [(int(img_length/3), int(img_width/2)), (int(img_length/3*2), int(img_width/2))]
    rsky_patches = []
    for loc in rsky_locations:
        rsky_patches.append(img[loc[0]:loc[0] + PATCH_SIZE,
                                loc[1]:loc[1] + PATCH_SIZE])

    # compute some GLCM properties each patch
    xs = []
    ys = []
    for patch in (grass_patches +sky_patches + rgrass_patches + rsky_patches):
        glcm = greycomatrix(patch, [5], [0], 256, symmetric=True, normed=True)
        xs.append(greycoprops(glcm, 'dissimilarity')[0, 0])
        ys.append(greycoprops(glcm, 'correlation')[0, 0])
    xsys = [xs[0], xs[1], xs[2], xs[3], xs[4], xs[5], ys[0], ys[1], ys[2], ys[3], ys[4], ys[5]]
    return xsys

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
args = vars(ap.parse_args())

# grab the list of images that we'll be describing
print("[INFO] describing images...")
imagePaths = list(paths.list_images(args["dataset"]))

# initialize the data matrix and labels list
data = []
labels = []
names = []

# loop over the input images
for (i, imagePath) in enumerate(imagePaths):
	# load the image and extract the class label (assuming that our
	# path as the format: /path/to/dataset/{class}.{image_num}.jpg
	image = cv2.imread(imagePath)
	label = imagePath.split(os.path.sep)[-1].split(".")[0]
	name2 = imagePath.split(os.path.sep)[-1].split("."[0])

	# extract a color histogram from the image, then update the
	# data matrix and labels list
	xsys = extract_glcm(image)
	data.append(xsys)
	labels.append(label)
	names.append(name2[1])

	# show an update every 1,000 images
	if i > 0 and i % 1000 == 0:
		print("[INFO] processed {}/{}".format(i, len(imagePaths)))

#print (names)
    ##print(labels)
    ## labels = bugbitten or unbugbitten
    ## image = hsv or rgb values - check
# encode the labels, converting them from strings to integers
le = LabelEncoder()
labels = le.fit_transform(labels)

# partition the data into training and testing splits, using 75%
# of the data for training and the remaining 25% for testing
print("[INFO] constructing training/testing split...")
(trainData, testData, trainLabels, testLabels, trainNames, testNames) = train_test_split(
	np.array(data), labels, names, test_size=0.50, random_state=42)

# train the linear regression clasifier
print("[INFO] training Linear SVM classifier...")
model = LinearSVC()
model.fit(trainData, trainLabels)

# evaluate the classifier
print("[INFO] evaluating classifier...")
predictions = model.predict(testData)
print(classification_report(testLabels, predictions,
	target_names=le.classes_))

##label = unbugbitten
##Predictions = predicted classification - check
##testLabels = actual classification - check
#len = length(testNames)

with open('output.csv', 'w') as f:
    writer = csv.writer(f)
    for counter, val in enumerate(testNames):
        writer.writerow([val, testLabels[counter], predictions[counter]])