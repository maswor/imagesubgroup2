The code I used came from 
https://www.pyimagesearch.com/2016/08/22/an-intro-to-linear-classification-with-python/
There is a good explanation of everything there.

The first classification that I have run just uses a histogram to classify the data. 
The output excel file contains the image number, class that the image was supposed to belong to, and the class that the program assigned to it.
The image number corresponds to the file name from the excel file called file_names.

The second classification uses texture to classify the images. I tried it with just classifying the images based on the grayscale image first but that wasn’t getting very high accuracy so I switched to classifying them based on both the grayscale and the red channel of color. This worked much better and is the code I included. 

Here is a link with the example code and an explanation of how it measures texture.
http://scikit-image.org/docs/dev/auto_examples/features_detection/plot_glcm.html

For the first classification, I only used 25% of the data to train on but for the second one, I had to use 50% of the data to train on. For both, the train and test data was randomly selected so running the code multiple times gives slightly different results. However, the overall accuracy is about the same every time.