"""
Residual Network implemented in Keras
Note: We only support Tensorflow backend + Python 3
"""
import warnings
from pathlib import Path
from keras import utils, layers, engine, models, backend as K
from imagenet_utils import _obtain_input_shape

WEIGHTS_PATH = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels.h5'
WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5'


def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2,
                                                                          2)):
    """
    Input: input_tensor: Keras tensor
           kernel_size: Number, ie 3 for middle Conv2D layer
           filters: List of 3 filters, eg: [64, 64, 256]
           stage, block: Used for generating layer's name, eg: 2, 'a'
           strides: strides for first Conv2D block, eg: (2, 2)
    Output: Keras tensor
    """
    filters1, filters2, filters3 = filters
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1

    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = layers.Conv2D(
        filters1, (1, 1), strides=strides,
        name=conv_name_base + '2a')(input_tensor)
    x = layers.BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = layers.Activation('relu')(x)

    x = layers.Conv2D(
        filters2, kernel_size, padding='same', name=conv_name_base + '2b')(x)
    x = layers.BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = layers.Activation('relu')(x)

    x = layers.Conv2D(filters3, (1, 1), name=conv_name_base + '2c')(x)
    x = layers.BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    shortcut = layers.Conv2D(
        filters3, (1, 1), strides=strides,
        name=conv_name_base + '1')(input_tensor)
    shortcut = layers.BatchNormalization(
        axis=bn_axis, name=bn_name_base + '1')(shortcut)

    x = layers.add([x, shortcut])
    x = layers.Activation('relu')(x)

    return x


def identity_block(input_tensor, kernel_size, filters, stage, block):
    """
    identity block: doens't have conv block at shortcut
    Input: input_tensor: keras tensor
           kernel_size: Number, ie 3 for middle Conv2D layer
           filters: List of 3 filters, eg: [64, 64, 256]
           stage, block: Used for generating layer's name, eg: 2, 'a'
    Output: Keras tensor
    """
    filters1, filters2, filters3 = filters
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = layers.Conv2D(
        filters1, (1, 1), name=conv_name_base + '2a')(input_tensor)
    x = layers.BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = layers.Activation('relu')(x)

    x = layers.Conv2D(
        filters2, kernel_size, padding='same', name=conv_name_base + '2b')(x)
    x = layers.BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = layers.Activation('relu')(x)

    x = layers.Conv2D(filters3, (1, 1), name=conv_name_base + '2c')(x)
    x = layers.BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    x = layers.add([x, input_tensor])
    x = layers.Activation('relu')(x)
    return x


def ResNet50(include_top=True,
             weights='imagenet',
             input_tensor=None,
             input_shape=None,
             pooling=None,
             classes=1000):
    """
    Construct ResNet50
    Input: weights: 'imagenet', None, or file_path
           input_tensor: optional keras tensor as input
           pooling: None, avg, max
           input_shape: img shape (ie. 224, 224, 3)
           class: number of class
    Output: Keras tensor
    """
    image_data_format = K.image_data_format()
    if not (weights in {'imagenet', None} or Path(weights).exists()):
        raise ValueError(
            'Invalid weights. Either \'imagenet\', \'None\' or valid weights file'
        )
    elif (weights == 'imagenet' and include_top and classes != 1000):
        raise ValueError('inclde_top require classes == 1000')
    input_shape = _obtain_input_shape(
        input_shape,
        default_size=224,
        min_size=197,
        data_format=image_data_format,
        require_flatten=include_top,
        weights=weights)
    if input_tensor is None:
        img_input = layers.Input(shape=input_shape)
    else:
        if not K.is_keras_tensor(input_tensor):
            img_input = layers.Input(tensor=input_tensor, shape=input_shape)
        else:
            img_input = input_tensor
    if image_data_format == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1

    x = layers.ZeroPadding2D(padding=(3, 3), name='conv1_pad')(img_input)
    x = layers.Conv2D(
        64, (7, 7), strides=(2, 2), padding='valid', name='conv1')(x)
    x = layers.BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
    x = layers.Activation('relu')(x)
    x = layers.MaxPooling2D((3, 3), strides=(2, 2))(x)

    x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

    x = conv_block(x, 3, [128, 128, 512], stage=3, block='a')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

    x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')

    x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')

    x = layers.AveragePooling2D((7, 7), name='avg_pool')(x)

    if include_top:
        x = layers.Flatten()(x)
        x = layers.Dense(classes, activation='softmax', name='fc1000')(x)
    elif pooling == 'avg':
        x = layers.GlobalAveragePooling2D()(x)
    elif pooling == 'max':
        x = layers.GlobalMaxPooling2D()(x)

    if input_tensor is not None:
        inputs = engine.topology.get_source_inputs(input_tensor)
    else:
        inputs = img_input

    model = models.Model(inputs, x, name='resnet50')
    if weights == 'imagenet':
        if include_top:
            weights_path = utils.get_file(
                'resnet50_weights_tf_dim_ordering_tf_kernels.h5',
                WEIGHTS_PATH,
                cache_subdir='models',
                hash_algorithm='auto',
                file_hash='a7b3fe01876f51b976af0dea6bc144eb')
        else:
            weights_path = utils.get_file(
                'resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5',
                WEIGHTS_PATH_NO_TOP,
                cache_subdir='models',
                hash_algorithm='auto',
                file_hash='a268eb855778b3df3c7506639542a6af')
        model.load_weights(weights_path)
        if K.image_data_format() == 'channels_first' and K.backend(
        ) == 'tensorflow':
            warnings.warn("Tensorflow prefer channels_last")
        if K.backend() != 'tensorflow':
            raise RuntimeError("We only support tensorflow backend")
    elif weights is not None:
        model.load_weights(weights)

    return model


class ResNet50MGPU(models.Model):
    """ Overload over keras multi-gpus """

    def __init__(self, ser_model, gpus):
        pmodel = utils.multi_gpu_model(ser_model, gpus)
        self.__dict__.update(pmodel.__dict__)
        self._smodel = ser_model

    def __getattribute__(self, attrname):
        if 'load' in attrname or 'save' in attrname:
            return getattr(self._smodel, attrname)
        return super(ResNet50MGPU, self).__getattribute__(attrname)
